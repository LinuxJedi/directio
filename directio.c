/*
Copyright (c) 2017, Andrew Hutchings <andrew@linuxjedi.co.uk>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define _GNU_SOURCE
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#define BLOCKSIZE 8192
#define BLOCKCOUNT 32*1024

double diff(struct timespec start, struct timespec end)
{
    double dtime = ( end.tv_sec - start.tv_sec )
        + ( end.tv_nsec - start.tv_nsec )
        / (double)1E9;

	return dtime;
}

void writeBench()
{
    struct timespec start, end;
    unsigned char *buffer;
    int f;
    posix_memalign((void**)&buffer, BLOCKSIZE, BLOCKSIZE);

    for (size_t i = 0; i < BLOCKSIZE - 1; i++)
    {
        int x = rand() % 256;
        buffer[i] = (unsigned char)x;
    }
    clock_gettime(CLOCK_MONOTONIC, &start);
    f = open("testfile.dat", O_CREAT|O_TRUNC|O_WRONLY|O_DIRECT, S_IRWXU);
    for (int i = 0; i < BLOCKCOUNT; i++)
    {
        write(f, buffer, BLOCKSIZE);
    }
    fsync(f);
    close(f);

    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("O_DIRECT Time: %f\n", diff(start,end));
    clock_gettime(CLOCK_MONOTONIC, &start);
    FILE *bf = fopen("testfile2.dat", "w");
    for (int i = 0; i < BLOCKCOUNT; i++)
    {
        fwrite(buffer, sizeof(char), BLOCKSIZE, bf);
    }
    fsync(f);
    posix_fadvise(f, 0, 0, POSIX_FADV_DONTNEED);
    fclose(bf);
    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("Regular Time: %f\n", diff(start,end));

    clock_gettime(CLOCK_MONOTONIC, &start);
    f = open("testfile3.dat", O_CREAT|O_TRUNC|O_RDWR, S_IRWXU);
    posix_fallocate(f, 0, BLOCKCOUNT*BLOCKSIZE);
    void *file_memory = mmap(NULL, BLOCKCOUNT*BLOCKSIZE, PROT_WRITE, MAP_SHARED, f, 0);
    if (file_memory == MAP_FAILED)
    {
        printf("Error: %d\n", errno);
        return;
    }
    void *pos = file_memory;
    for (int i = 0; i < BLOCKCOUNT; i++)
    {
        memcpy(pos, buffer, BLOCKSIZE);
        pos += BLOCKSIZE;
    }
    munmap(file_memory, BLOCKCOUNT*BLOCKSIZE);
    fsync(f);
    posix_fadvise(f, 0, 0, POSIX_FADV_DONTNEED);
    close(f);
    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("mmap Time: %f\n", diff(start,end));

    clock_gettime(CLOCK_MONOTONIC, &start);
    f = open("testfile4.dat", O_CREAT|O_TRUNC|O_RDWR, S_IRWXU);
    posix_fallocate(f, 0, BLOCKCOUNT*BLOCKSIZE);
    for (int i = 0; i < BLOCKCOUNT; i++)
    {
        file_memory = mmap(NULL, BLOCKSIZE, PROT_WRITE, MAP_SHARED, f, i*BLOCKSIZE);
        if (file_memory == MAP_FAILED)
        {
            printf("Error: %d\n", errno);
            return;
        }
        memcpy(file_memory, buffer, BLOCKSIZE);
        munmap(file_memory, BLOCKSIZE);
    }
    fsync(f);
    posix_fadvise(f, 0, 0, POSIX_FADV_DONTNEED);
    close(f);
    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("mmap small Time: %f\n", diff(start,end));

    free(buffer);
}

void writeBenchBig()
{
    struct timespec start, end;
    unsigned char *buffer;
    int f;
    posix_memalign((void**)&buffer, BLOCKCOUNT*BLOCKSIZE, BLOCKCOUNT*BLOCKSIZE);

    for (size_t i = 0; i < BLOCKCOUNT*BLOCKSIZE - 1; i++)
    {
        int x = rand() % 256;
        buffer[i] = (unsigned char)x;
    }
    clock_gettime(CLOCK_MONOTONIC, &start);
    f = open("testfile.dat", O_CREAT|O_TRUNC|O_WRONLY|O_DIRECT, S_IRWXU);
    write(f, buffer, BLOCKSIZE*BLOCKCOUNT);
    fsync(f);
    close(f);

    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("O_DIRECT Time: %f\n", diff(start,end));
    clock_gettime(CLOCK_MONOTONIC, &start);
    FILE *bf = fopen("testfile2.dat", "w");
    fwrite(buffer, sizeof(char), BLOCKSIZE*BLOCKCOUNT, bf);
    fsync(f);
    posix_fadvise(f, 0, 0, POSIX_FADV_DONTNEED);
    fclose(bf);
    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("Regular Time: %f\n", diff(start,end));

    clock_gettime(CLOCK_MONOTONIC, &start);
    f = open("testfile3.dat", O_CREAT|O_TRUNC|O_RDWR, S_IRWXU);
    posix_fallocate(f, 0, BLOCKCOUNT*BLOCKSIZE);
    void *file_memory = mmap(NULL, BLOCKCOUNT*BLOCKSIZE, PROT_WRITE, MAP_SHARED, f, 0);
    if (file_memory == MAP_FAILED)
    {
        printf("Error: %d\n", errno);
        return;
    }
    memcpy(file_memory, buffer, BLOCKSIZE*BLOCKCOUNT);
    munmap(file_memory, BLOCKCOUNT*BLOCKSIZE);
    fsync(f);
    posix_fadvise(f, 0, 0, POSIX_FADV_DONTNEED);
    close(f);
    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("mmap Time: %f\n", diff(start,end));

    free(buffer);
}


void readBench()
{
    struct timespec start, end;
    unsigned char *buffer;
    int f;
    posix_memalign((void**)&buffer, BLOCKSIZE, BLOCKSIZE);

    clock_gettime(CLOCK_MONOTONIC, &start);
    f = open("testfile.dat", O_RDONLY|O_DIRECT);
    for (int i = 0; i < BLOCKCOUNT; i++)
    {
        read(f, buffer, BLOCKSIZE);
    }
    close(f);

    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("O_DIRECT Time: %f\n", diff(start,end));
    clock_gettime(CLOCK_MONOTONIC, &start);
    FILE *bf = fopen("testfile2.dat", "r");
    for (int i = 0; i < BLOCKCOUNT; i++)
    {
        fread(buffer, sizeof(char), BLOCKSIZE, bf);
    }
    posix_fadvise(f, 0, 0, POSIX_FADV_DONTNEED);
    fclose(bf);
    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("Regular Time: %f\n", diff(start,end));

    clock_gettime(CLOCK_MONOTONIC, &start);
    f = open("testfile3.dat", O_RDONLY);
    void *file_memory = mmap(NULL, BLOCKCOUNT*BLOCKSIZE, PROT_READ, MAP_SHARED, f, 0);
    if (file_memory == MAP_FAILED)
    {
        printf("Error: %d\n", errno);
        return;
    }
    void *pos = file_memory;
    for (int i = 0; i < BLOCKCOUNT; i++)
    {
        memcpy(buffer, pos, BLOCKSIZE);
        pos += BLOCKSIZE;
    }
    munmap(file_memory, BLOCKCOUNT*BLOCKSIZE);
    posix_fadvise(f, 0, 0, POSIX_FADV_DONTNEED);
    close(f);
    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("mmap Time: %f\n", diff(start,end));

    clock_gettime(CLOCK_MONOTONIC, &start);
    f = open("testfile4.dat", O_RDONLY);
    for (int i = 0; i < BLOCKCOUNT; i++)
    {
        file_memory = mmap(NULL, BLOCKSIZE, PROT_READ, MAP_SHARED, f, i*BLOCKSIZE);
        if (file_memory == MAP_FAILED)
        {
            printf("Error: %d\n", errno);
            return;
        }
        memcpy(buffer, file_memory, BLOCKSIZE);
        munmap(file_memory, BLOCKSIZE);
    }
    posix_fadvise(f, 0, 0, POSIX_FADV_DONTNEED);
    close(f);
    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("mmap small Time: %f\n", diff(start,end));

    free(buffer);
}

void readBenchBig()
{
    struct timespec start, end;
    unsigned char *buffer;
    int f;
    posix_memalign((void**)&buffer, BLOCKCOUNT*BLOCKSIZE, BLOCKCOUNT*BLOCKSIZE);

    clock_gettime(CLOCK_MONOTONIC, &start);
    f = open("testfile.dat", O_RDONLY|O_DIRECT);
    read(f, buffer, BLOCKCOUNT*BLOCKSIZE);
    close(f);

    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("O_DIRECT Time: %f\n", diff(start,end));
    clock_gettime(CLOCK_MONOTONIC, &start);

    FILE *bf = fopen("testfile2.dat", "r");
    fread(buffer, sizeof(char), BLOCKCOUNT*BLOCKSIZE, bf);
    posix_fadvise(f, 0, 0, POSIX_FADV_DONTNEED);
    fclose(bf);
    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("Regular Time: %f\n", diff(start,end));

    clock_gettime(CLOCK_MONOTONIC, &start);
    f = open("testfile3.dat", O_RDONLY);
    void *file_memory = mmap(NULL, BLOCKCOUNT*BLOCKSIZE, PROT_READ, MAP_SHARED, f, 0);
    if (file_memory == MAP_FAILED)
    {
        printf("Error: %d\n", errno);
        return;
    }
    memcpy(buffer, file_memory, BLOCKSIZE*BLOCKCOUNT);
    munmap(file_memory, BLOCKCOUNT*BLOCKSIZE);
    posix_fadvise(f, 0, 0, POSIX_FADV_DONTNEED);
    close(f);
    clock_gettime(CLOCK_MONOTONIC, &end);
    printf("mmap Time: %f\n", diff(start,end));

    free(buffer);
}




int main()
{
    printf("Write bench\n");
    writeBench();
    printf("Read bench\n");
    readBench();
    printf("WriteBig bench\n");
    writeBenchBig();
    printf("ReadBig bench\n");
    readBenchBig();
    return 0;
}

